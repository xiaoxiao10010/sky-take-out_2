package com.sky.task;

import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@Slf4j
public class OrderTask {
    @Autowired
    private OrdersMapper ordersMapper;

    /**
     * 处理支付超时订单
     */
    @Scheduled(cron = "1 * * * * ?")
    // @Scheduled(cron = "5/10 * * * * ?")
    public void processTimeoutOrder() {

        LocalDateTime localDateTime = LocalDateTime.now().plusMinutes(-15);
        List<Orders> list = ordersMapper.getByStatusAndOrdertimeLT(Orders.PENDING_PAYMENT, localDateTime);

        if (list != null && list.size() > 0) {
            list.stream().forEach((l) -> {
                l.setStatus(Orders.CANCELLED);
                l.setCancelReason("支付超时，自动取消");
                l.setCancelTime(LocalDateTime.now());
                ordersMapper.update(l);
            });
        }


    }

    /**
     * 处理“派送中”状态的订单
     */
    @Scheduled(cron = "0 0 1 * * ?")
    // @Scheduled(cron = "0/5 * * * * ?")
    public void processDeliveryOrder() {
        LocalDateTime localDateTime = LocalDateTime.now().plusMinutes(-60);
        List<Orders> list = ordersMapper.getByStatusAndOrdertimeLT(Orders.DELIVERY_IN_PROGRESS, localDateTime);

        if (list != null && list.size() > 0) {
            list.stream().forEach((l) -> {
                l.setStatus(Orders.COMPLETED);
                ordersMapper.update(l);
            });
        }


    }
}
